<?php   
	require("assets/php/account/common.php"); 
?>
<!DOCTYPE html>
<!--
	Typify by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head>
	<title>TXT PP2P</title>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	<link href="assets/css/main.css" rel="stylesheet">
	<link href="assets/css/toastr.css" rel="stylesheet">
	<script src="assets/js/peer.js">
	</script>
	<script src="assets/js/jquery.min.js">
	</script>
	<script src="assets/js/skel.min.js">
	</script>
	<script src="assets/js/util.js">
	</script>
	<script src="assets/js/main.js">
	</script>
	<script src="assets/js/toastr.js">
	</script>
	<script src="assets/js/login.js" type="text/javascript">
	</script>
	<script>
		<?php
			if(empty($_SESSION['user'])){ 
				echo "var peer = new Peer( {host: '109.95.200.172', port: 9000, path: '/peerjs'});";
			}
			if(($_SESSION['user'])){
				echo "var peer = new Peer('";echo $_SESSION['user']['peerid']; echo "',{host: '109.95.200.172', port: 9000, path: '/peerjs'});";
			}
		?>
		var gid;
		peer.on('open', function(id)
		{
			$('#pid').text(id);
			gid = id;
			document.getElementById('peerid').value = id;
			$('#peerid').text(id);
		});  
		var made_conn = false;
		var peerName;
		// Await connections from others
		peer.on('connection', connect);
		function connect(c) 
		{
			peerName = c.metadata.userName;
			$('#chat_area').show();
			$('#li_disconnect').show();
			$('#li_connect').hide();
			$('#connections').hide();
			conn = c;

			conn.on('data', function(data)
			{
				var n = data.search(/MYNAMEISTHIS/i);
				if(n == 0)
					{
						var res = data.split(":"); 
						peerName = res[1];
					}
					else
				{
				$('#messages').append('<br>' + peerName + ':<br>' + data);
				}
			});
			
			if (made_conn)
			{
				setTimeout(function(){
					$('#messages').empty().append('Now chatting with ' + peerName);
				}, 500);
				
			}
			else 
			{
				//my name is...
				<?php
					if(empty($_SESSION['user'])){ 
						echo "var myUserName = gid;";
					}
					if(($_SESSION['user'])){
						echo "var myUserName =  '"; echo $_SESSION['user']['username']; echo "';";
					}
				?>
				setTimeout(function(){
								var temp = "MYNAMEISTHIS:" + myUserName;
								conn.send(temp);
				}, 300);

				$('#messages').empty().append('Now chatting with ' + peerName);
				
			}

			c.on('close', function(err)
			{ 
				toastr["info"]( conn.peer + ' has disconnected.');
				$('#chat_area').hide();
				$('#li_disconnect').hide();
				$('#connections').show();
				$('#li_connect').show();
			});
		}
		 
		$(document).ready(function() {
			// Handle connection
			$('#connect').click(function()
			{
				<?php
					if(empty($_SESSION['user']))
					{ 
						echo "var myUserName = gid;";
					}
					if(($_SESSION['user']))
					{
						echo "var myUserName =  '"; echo $_SESSION['user']['username']; echo "';";
					}
				?>
				var c = peer.connect($('#rid').val(),{metadata: { userName: myUserName } });
				c.on('open', function()
				{
					made_conn = true;
					connect(c);
				});
				c.on('error', function(err){ alert(err) });  
			});         
			// Send a chat message
			$('#send').click(function()
			{
				var msg = $('#text').val();
				conn.send(msg);
				$('#messages').append('<br>You:<br>' + msg);
				$('#text').val('');
			});
			
			$('#close').click(function() 
			{
				conn.close();
			});
		 });
		 
		window.onunload = window.onbeforeunload = function(e) 
		{
			if (!!peer && !peer.destroyed) {
				peer.destroy();
			}
		};
	</script>
</head>
<body>
	<section id="banner">
		<ul class="actions">
			<li>
				<a class="button logo" href="index.php"><strong>PP2P</strong></a>
			</li>
			<li>
				<a class="button special" href="file.php">send files</a>
			</li>
			<li>
				<a class="button special:active" href="#">chat</a>
			</li>
			<li>
				<a class="button special" href="video.php">video chat</a>
			</li>
			<li>
				<a class="button special" href="http://329elearning.aei.polsl.pl/tiwordpress2015/s316/">blog</a>
			</li>
			<li><input id="show_menu" src="/images/hamburger.png" type="image" value="Show Menu">
			<?php	require("assets/php/menu.php"); ?></li>
		</ul>
	</section>
	<section class="wrapper special" id="one">
		<div class="row">
			<div class="col-1">
				<ul>
					<li>Your ID is <span id="pid"></span></li>
					<?php 
					require("assets/php/friends/get_friend.php"); 
					if($count == 0)
					{
						echo '<li id="li_connect">Connect to a peer: <input type="text" id="rid" placeholder="Other person PeerID">';
						}
						else
						{
							echo '<li id="li_connect">
								<input list="peers" id="rid" placeholder="Type PeerID or select from your friend list">
								<datalist id="peers">';
								for ($i = 0; $i < $count; $i++) 
								{
									echo '<option value="'; 
									echo $friend_list[$i]['peerid']; 
									echo '" label="'; 
									echo $friend_list[$i]['username'] ;
									echo '">';
								}
						echo '</datalist>';
						}
					?>
					<li style="list-style: none"><input class="button" id="connect" type="button" value="Connect"></li>
					<li id="li_disconnect" style="display: none;"><button id="close">Close connection</button></li>
				</ul>
			</div>
			<div class="col-2">
				<div id="wrap">
					<div id="connections">
						<span class="filler">You have not yet made any connections.</span>
					</div>
					<div class="clear"></div>
					<div id="chat_area" style="display: none;">
						<div id="messages"></div><input id="text" placeholder="Enter message" type="text"> <input id="send" type="button" value="Send">
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>
