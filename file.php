<?php   
	require("assets/php/account/common.php"); 
?>
<!DOCTYPE html>
<!--
	Typify by TEMPLATED
	templated.co @templatedco
	Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<!--
TODO: 
prawdopodobnie obsluga jakiegos kanalu zeby przesylac informacje o nazwie i typie pliku
moze progress bar, ale to wymaga dzielenia plikow na czesci
https://www.npmjs.com/package/peer-file ==to jest ciekawe
https://www.google.pl/search?q=peerjs+transfer+file+as+blob&ie=utf-8&oe=utf-8&gws_rd=cr&ei=fUxKVsi8M6nnywPGr4ToAg
-->
<html>
<head>
	<title>DATA PP2P</title>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1" name="viewport">
	<link href="assets/css/main.css" rel="stylesheet">
	<link href="assets/css/toastr.css" rel="stylesheet">
	<script src="assets/js/peer.js">
	</script>
	<script src="assets/js/jquery.min.js">
	</script>
	<script src="assets/js/skel.min.js">
	</script>
	<script src="assets/js/util.js">
	</script>
	<script src="assets/js/main.js">
	</script>
	<script src="assets/js/FileSaver.js">
	</script>
	<script src="assets/js/toastr.js">
	</script>
	<script src="assets/js/login.js" type="text/javascript">
	</script>
	<script>

		   <?php
		   if(empty($_SESSION['user'])){ 
		   echo "var peer = new Peer( {host: '109.95.200.172', port: 9000, path: '/peerjs'});";
		   }
		   if(($_SESSION['user'])){
		   echo "var peer = new Peer('";echo $_SESSION['user']['peerid']; echo "',{host: '109.95.200.172', port: 9000, path: '/peerjs'});";
		   }
		   ?>
		   var gid;
		 peer.on('open', function(id) {
			 $('#pid').text(id);
			 gid = id;
			 document.getElementById('peerid').value = id;
			 $('#peerid').text(id);
		 });
		  // Await connections from others
		 peer.on('connection', connect);
		 peer.on('error', function(err) {
			 console.log(err);
		 });
		  // Handle a connection object.
		 function connect(c) {
			 // Handle a chat connection.
			 if (c.label === 'chat') {
				 conn = c;
				 $('#file_area').show();
				 $('#li_disconnect').show();
				 $('#li_connect').hide();
				 $('#connections').hide();
				 $('#messages').show();
				 $('#messages').empty().append('Data transfer with ' + conn.peer);
				 c.on('data', function(data) {
					 console.log(data);
					 var res = data.split("===");
					 filename = res[0];
					 size = res[1];
				 });
				 c.on('close', function(err) {
					 toastr["info"](conn.peer + ' has disconnected.');
					 $('#file_area').hide();
					 $('#li_disconnect').hide();
					 $('#connections').show();
					 $('#li_connect').show();
				 });
			 } else if (c.label === 'file') {
				 fconn = c;
				 c.on('data', function(data) {
					 // If we're getting a file, create a URL for it.
					 if (data.constructor === ArrayBuffer) {
						 $('#box').hide();
						 var dataView = new Uint8Array(data);
						 var dataBlob = new Blob([dataView]);
						 var url = window.URL.createObjectURL(dataBlob);
						 //TODO url nie pokazuje sie w oknie czatu
						 $('#messages').empty().append(
							 '<div><span class="file">' + c.peer +
							 ' has sent you a <input type="button" value="' +
							 filename + '" id="readyFile">size: ' +
							 size + '<\/span><\/div>');
						 $("#readyFile").click(function() {
							 saveAs(dataBlob, filename);
						 });
					 }
				 });
			 }
		 }
		 $(document).ready(function() {
			 //Prepare file drop box.
			 //TODO: inny picker plikow
			 var box = $('#box');
			 //box.on('drop', sendFile(e));
			 $(box).on("dragover drop", function(e) {
				 e.preventDefault();
			 }).on("drop", function(e) {
				 sendFile(e);
			 });

			 function sendFile(e) {
				 e.originalEvent.preventDefault();
				 var file = e.originalEvent.dataTransfer.files[0];
				 console.log("name: " + file.name);
				 console.log("size: " + file.size);
				 conn.send(file.name + "===" + file.size);
				 fconn.send(file);
				 $('#messages').empty().append(
					 'Trying to send file to ' + conn.peer + '...'
				 );
				 console.log(conn.peer + ' iiii ');
			 };

			 function doNothing(e) {
					 e.preventDefault();
					 e.stopPropagation();
				 }
				 // Connect to a peer
			 $('#connect').click(function() {
				 var requestedPeer = $('#rid').val();
				 // Create 1 connection labelled file.
				 var f = peer.connect(requestedPeer, {
					 label: 'file',
					 reliable: true
				 });
				 f.on('open', function() {
					 connect(f);
				 });
				 f.on('error', function(err) {
					 alert(err);
				 }); 
				 <?php
				 if (empty($_SESSION['user'])) {
					 echo "var myUserName = gid;";
				 }
				 if (($_SESSION['user'])) {
					 echo "var myUserName =  '";
					 echo $_SESSION['user']['username'];
					 echo "';";
				 } 
				 ?>
				 var c = peer.connect(requestedPeer, {
					 label: 'chat',
					 metadata: {
						 userName: myUserName
					 },
					 reliable: true
				 });
				 c.on('open', function() {
					 connect(c);
				 });
				 c.on('error', function(err) {
					 alert(err);
				 });
			 });
			 // Close a connection.
			 $('#close').click(function() {
				 conn.close();
			 });
		 });
		  // Make sure things clean up properly.
		 window.onunload = window.onbeforeunload = function(e) {
			 if (!!peer && !peer.destroyed) {
				 peer.destroy();
			 }
		 };
	</script>
</head>
<body>
	<section id="banner">
		<ul class="actions">
			<li>
				<a class="button logo" href="index.php"><strong>PP2P</strong></a>
			</li>
			<li>
				<a class="button special:active" href="#">send files</a>
			</li>
			<li>
				<a class="button special" href="txt.php">chat</a>
			</li>
			<li>
				<a class="button special" href="video.php">video chat</a>
			</li>
			<li>
				<a class="button special" href="http://329elearning.aei.polsl.pl/tiwordpress2015/s316/">blog</a>
			</li>
			<li><input id="show_menu" src="/images/hamburger.png" type="image" value="Show Menu"> <?php   require("assets/php/menu.php"); ?></li>
		</ul>
	</section>
	<section class="wrapper special" id="one">
		<div class="row">
			<div class="col-1">
				<ul>
					<li>Your ID is <span id="pid"></span></li>
					<?php 
						require("assets/php/friends/get_friend.php"); 
						if($count == 0)
						{
							echo '	<li id="li_connect">Connect to a peer: <input type="text" id="rid" placeholder="Other person PeerID">';
						}
						else
						{
							echo '	<li id="li_connect">
									<input list="peers" id="rid" placeholder="Type PeerID or select from your friend list">
									<datalist id="peers">';
						for ($i = 0; $i < $count; $i++) 
						{
							echo '	<option value="'; echo $friend_list[$i]['peerid']; echo '" label="'; echo $friend_list[$i]['username'] ;echo '">';
						}
						echo '</datalist>';
						}
					?>
					<li style="list-style: none"><input class="button" id="connect" type="button" value="Connect"></li>
					<li id="li_disconnect" style="display: none;"><button id="close">Close connection</button></li>
				</ul>
			</div>
			<div class="col-2">
				<div id="wrap">
					<div id="connections">
						<span class="filler">You have not yet made any connections.</span>
					</div>
					<div class="clear"></div>
					<div id="file_area" style="display: none;">
						Select file to send:
						<div id="box" style="background: #7E8082; font-size: 18px; padding: 40px 30px; text-align: center; width: 50%; border-radius: 30px; position: absolute; left:37%; top: 125%;">
							Drag file here to send to active connection.<br>
						</div>
						<div id="messages" style="display: none;"></div>
					</div>
				</div>
			</div>
		</div>
	</section>
</body>
</html>
