create database pp2p;
insert into mysql.user (User, Host, Password) Values ('pp2p','localhost',PASSWORD('PP2P9('));
select user from mysql.user;
grant all privileges on pp2p.* to pp2p@localhost;
FLUSH PRIVILEGES;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` char(64) COLLATE utf8_unicode_ci NOT NULL,
  `salt` char(16) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `peerid` char(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci



CREATE TABLE `friends` (
  `id` int(11) NOT NULL,
  `friend` int(11) DEFAULT NULL,
  `username` varchar(60) DEFAULT NULL,
  `peerid` char(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1

