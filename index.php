<?php   
	require("assets/php/account/common.php"); 
?>
<!DOCTYPE html>
<html>
	<head>
		<title>
			PP2P
		</title>
		<meta charset="utf-8">
		<meta content="width=device-width, initial-scale=1" name="viewport">
		<link href="assets/css/main.css" rel="stylesheet">
		<script src="assets/js/jquery.min.js">
		</script>
		<script src="assets/js/login.js">
		</script>
		<script src="assets/js/peer.js">
		</script>
		<script src="assets/js/jquery.min.js">
		</script>
		<script>
			var peer = new Peer( {host: '109.95.200.172', port: 9000, path: '/peerjs'});
			peer.on('open', function(id){
				document.getElementById('peerid').value = id;
				$('#peerid').text(id);
			});  

			 $(document).ready(function() {
				 			peer.on('error', function()
			{
				$('#messages').empty().append("First, you must visit <a href='https://109.95.200.172:9000/peerjs'>this link</a>, and accept SSL certificate.");
				$('#messages').append("<br>If you done that, and P2PP still doesn't work for you, it's probably our fault. Please wait until we fix that.");
			});
				}); 
		</script>
	</head>
	<body>
		<section id="banner">
			<ul class="actions">
				<li>
					<a class="button logo" href="#"><strong>PP2P</strong></a>
				</li>
				<li>
					<a class="button special" href="file.php">send files</a>
				</li>
				<li>
					<a class="button special" href="txt.php">chat</a>
				</li>
				<li>
					<a class="button special" href="video.php">video chat</a>
				</li>
				<li>
					<a class="button special" href=
					"http://329elearning.aei.polsl.pl/tiwordpress2015/s316/">blog</a>
				</li>
				<li>
					<input id="show_menu" src="/images/hamburger.png" type=
					"image" value="Show Menu">
					<?php   require("assets/php/menu.php"); ?>
				</li>
			</ul>
		</section>
		<section class="wrapper special" id="one">
			<div class="inner">
				<header class="major">
					<h2>
						<strong><div id="messages">SOON...</div></strong>
					</h2>
				</header>
			</div>
		</section>
	</body>
</html>
