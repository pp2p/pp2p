<?php 		
	require("../account/common.php"); 

	
	        $query = "
            SELECT
                id,
                username,
                peerid
            FROM users
            WHERE
                username = :friend
        ";

        $query_params = array(
            ':friend' => $_POST['friend']
        );
        
        try
        {
            $stmt = $db->prepare($query);
            $result = $stmt->execute($query_params);
        }
        catch(PDOException $ex)
        {
            die("Failed to run query: " . $ex->getMessage());
        }
        
        $row = $stmt->fetch();

        if($row)
        {
			if($row['id'] == $_SESSION['user'][id])
			{
				$message = 'Cant add yourself.';
				$_SESSION["message"]=$message;
				header("Location: /friends.php");
				die("Redirecting to: /friends.php"); 
			}
			else
			{
				$query = "
					SELECT
						1
					FROM friends
					WHERE
						id = :id
						AND
						friend = :friend
				";

				$query_params = array(
					':id' => $_SESSION['user'][id],
					':friend' => $row['id']
					);
				try
				{
					$stmt = $db->prepare($query);
					$result = $stmt->execute($query_params);
				}
				catch(PDOException $ex)
				{
					die("Failed to run query: " . $ex->getMessage());
				}
				$row_test = $stmt->fetch();
                if($row_test)
				{
					$message = 'This user is already on your friend list.';
					$_SESSION["message"]=$message;
					header("Location: /friends.php");
					die("Redirecting to: /friends.php"); 
				}
			}
		
        }
		else 
		{
			$message = 'Cant find that user';
			$_SESSION["message"]=$message;
			header("Location: /friends.php");
			die("Redirecting to: /friends.php"); 
		}
			
	    $query = "
            INSERT INTO friends (
                id,
                friend,
                username,
                peerid
            ) VALUES (
                :id,
                :friend,
                :username,
                :peerid
            )
        ";

		$query_params = array(
            ':id' => $_SESSION['user'][id],
            ':friend' => $row['id'],
            ':username' => $row['username'],
            ':peerid' => $row['peerid']
        );
                try
        {
            $stmt = $db->prepare($query);
            $result = $stmt->execute($query_params);
        }
        catch(PDOException $ex)
        {
            die("Failed to run query: " . $ex->getMessage());
        }
        $message = 'Friend added.';
		$_SESSION["message"]=$message;
		header("Location: /friends.php");
		die("Redirecting to: /friends.php"); 
?> 



