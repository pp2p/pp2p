<?php 	
	require("../account/common.php"); 
	
	        $query = "
            SELECT
                id,
                friend,
                username,
                peerid
            FROM friends
            WHERE
                username = :friend
                AND id = :id
        ";

        $query_params = array(
            ':friend' => $_POST['friend'],
            ':id' => $_SESSION['user'][id]
        );
        
        try
        {
            $stmt = $db->prepare($query);
            $result = $stmt->execute($query_params);
        }
        catch(PDOException $ex)
        {
            die("Failed to run query: " . $ex->getMessage());
        }
        
        $row = $stmt->fetch();

        if(!$row)
        {
				$message = 'Not found.';
				$_SESSION["message"]=$message;
				header("Location: /friends.php");
				die("Redirecting to: /friends.php"); 
			}
			
	    $query = "
            DELETE FROM friends 
            WHERE
				id = :id
				AND friend = :friend
				
        ";
		$query_params = array(
            ':id' => $_SESSION['user'][id],
            ':friend' => $row['friend'],
        );
                try
        {
            $stmt = $db->prepare($query);
            $result = $stmt->execute($query_params);
        }
        catch(PDOException $ex)
        {
            die("Failed to run query: " . $ex->getMessage());
        }
        $message = 'Friend deleted.';
		$_SESSION["message"]=$message;
		header("Location: /friends.php");
		die("Redirecting to: /friends.php"); 
?> 



