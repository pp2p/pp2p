<?php
	require("assets/php/account/common.php");
?>
<!DOCTYPE html>
<!--
Typify by TEMPLATED
templated.co @templatedco
Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title>
			VIDEO PP2P
		</title>
		<meta charset="utf-8">
		<meta content="width=device-width, initial-scale=1" name="viewport">
		<link href="assets/css/main.css" rel="stylesheet">
		<link href="assets/css/toastr.css" rel="stylesheet">
		<script src="assets/js/peer.js">
		</script>
		<script src="assets/js/jquery.min.js">
		</script>
		<script src="assets/js/skel.min.js">
		</script>
		<script src="assets/js/util.js">
		</script>
		<script src="assets/js/main.js">
		</script>
		<script src="assets/js/toastr.js">
		</script>
		<script src="assets/js/login.js" type="text/javascript">
		</script>
		<script>
		<?php
			if(empty($_SESSION['user'])){
				echo "var peer = new Peer( {host: '109.95.200.172', port: 9000, path: '/peerjs'});";
			}
			if(($_SESSION['user'])){
				echo "var peer = new Peer('";echo $_SESSION['user']['peerid']; echo "',{host: '109.95.200.172', port: 9000, path: '/peerjs'});";
			}
		?>
		navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
		peer.on('open', function(id) {
			$('#pid').text(id);
			document.getElementById('peerid').value = id;
			$('#peerid').text(id);
		});
		// Receiving a call
		peer.on('call', function(call) {
			call.answer(window.localStream);
			step3(call);
		});
		peer.on('error', function(err) {
			alert(err.message);
			// Return to step 2 if error occurs
			step2();
		});
		// Click handlers setup
		$(function() {
			$('#connect').click(function() {
				// Initiate a call!
				//TODO moze tutaj wyslac co udostepniamy? metadata
				var call = peer.call($('#rid').val(), window.localStream);
				step3(call);
			});
			$('#close').click(function() {
				window.existingCall.close();
				step2();
			});
			// Retry if getUserMedia fails
			$('#step1-retry').click(function() {
				$('#step1-error').hide();
				step1();
			});
			// Get things started
			step1();
		});

		function step1() {
			$('#step3').hide();
			// Get audio/video stream
			navigator.getUserMedia({
				audio: true,
				video: true
			}, function(stream) {
				// Set your video displays
				$('#my-video').prop('src', URL.createObjectURL(stream));
				window.localStream = stream;
				step2();
			}, function() {
				$('#step1-error').show();
			});
		}

		function step2() {
			$('#step1, #step3').hide();
			$('#step2').show();
		}

		function step3(call) {
			// Hang up on an existing call if present
			if (window.existingCall) {
				window.existingCall.close();
			}
			// Wait for stream on the call, then set peer video display
			call.on('stream', function(stream) {
				$('#peer-video').prop('src', URL.createObjectURL(stream));
			});
			// UI stuff
			window.existingCall = call;
			$('#rid').text(call.peer);
			call.on('close', step2);
			$('#step1, #step2').hide();
			$('#step3').show();
		}
		</script>
	</head>
	<body>
		<section id="banner">
			<ul class="actions">
				<li>
					<a class="button logo" href="index.php"><strong>PP2P</strong></a>
				</li>
				<li>
					<a class="button special" href="file.php">send files</a>
				</li>
				<li>
					<a class="button special" href="txt.php">chat</a>
				</li>
				<li>
					<a class="button special:active" href="#">video chat</a>
				</li>
				<li>
					<a class="button special" href=
					"http://329elearning.aei.polsl.pl/tiwordpress2015/s316/">blog</a>
				</li>
				<li>
					<input id="show_menu" src="/images/hamburger.png" type=
					"image" value="Show Menu">
					<?php   require("assets/php/menu.php"); ?>
				</li>
			</ul>
		</section>
		<section class="wrapper special" id="one">
			<div class="row">
				<div class="col-1">
					<div id="step2">
						<ul>
							<li>Your ID is <span id="pid"></span>
							</li>
							<?php 
								require("assets/php/friends/get_friend.php");
								if($count == 0)
								{
									echo '<li id="li_connect">Connect to a peer: <input type="text" id="rid" placeholder="Other person PeerID">';
								}
								else
									{
										echo '	<li id="li_connect">
												<input list="peers" id="rid" placeholder="Type PeerID or select from your friend list">
												<datalist id="peers">';
										for ($i = 0; $i < $count; $i++) {
											echo '<option value="'; echo $friend_list[$i]['peerid']; echo '" label="'; echo $friend_list[$i]['username'] ;echo '">';
										}
										echo '	</datalist>';
									}
							?>
							<li style="list-style: none">
								<input class="button" id="connect" type=
								"button" value="Call">
							</li>
							<li id="li_disconnect" style="display: none;">
								<button id="close">Close connection</button>
							</li>
						</ul>
					</div>
				</div>
				<div id="step3">
				<div class="col-2">
					<div class="pure-u-2-3" id="video-container">
						<video autoplay="" id="peer-video"></video> Your
						camera:<br>
						<video autoplay="" id="my-video"></video>
					</div>
				</div>
				</div>
			</div>
		</section>
	</body>
</html>
