<?php	
	require("assets/php/account/common.php"); 
?>
<!DOCTYPE html>
<html>
	<head>
		<title>
			PP2P
		</title>
		<meta charset="utf-8">
		<meta content="width=device-width, initial-scale=1" name="viewport">
		<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
		<link href="assets/css/main.css" rel="stylesheet">
		<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->

		<script src="assets/js/jquery.min.js">
		</script>
		<script src="assets/js/login.js" type="text/javascript">
		</script>
		<script src="assets/js/peer.js">
		</script>
		<script src="assets/js/jquery.min.js">
		</script>
		<script>
			var peer = new Peer( {host: '109.95.200.172', port: 9000, path: '/peerjs'});
			peer.on('open', function(id)
			{
				document.getElementById('peerid').value = id;
				$('#peerid').text(id);
			});  
		</script>
	</head>
	<body>
		<section id="banner">
			<ul class="actions">
				<li>
					<a class="button logo" href="index.php"><strong>PP2P</strong></a>
				</li>
				<li>
					<a class="button special" href="file.php">send files</a>
				</li>
				<li>
					<a class="button special" href="txt.php">chat</a>
				</li>
				<li>
					<a class="button special" href="audio.php">voice chat</a>
				</li>
				<li>
					<a class="button special" href="video.php">video chat</a>
				</li>
				<li>
					<a class="button special" href=
					"http://329elearning.aei.polsl.pl/tiwordpress2015/s316/">blog</a>
				</li>
				<li>
					<input id="show_menu" src="/images/hamburger.png" type=
					"image" value="Show Menu">
					<?php	require("assets/php/menu.php"); ?>
				</li>
			</ul>
		</section>
		<section class="wrapper special" id="one">
			<div class="inner">
				<?php
					if(empty($_SESSION['user']))
					{ 
						echo '<br><h1>FIRST SIGN IN, OR REGISTER</h1>';
					}
					if(($_SESSION['user']))
					{
						echo '	<form action="assets/php/friends/add_friend.php" method="post">
								<br>
								<h1>Add friend:</h1>
								<input type="text" name="friend" placeholder = "Friend name:" value=""/>
								<input type="submit" value="Add friend" />
								</form>
								<form action="assets/php/friends/delete_friend.php" method="post">
								<br><h1>Delete friend:</h1>
								<input type="text" name="friend" placeholder = "Friend name:" value=""/>
								<input type="submit" value="Delete friend" />
								</form>';
						echo '<h1>'; echo $_SESSION["message"]; echo '</h1>';
						echo '<br><br><h1>list of your friends:</h1>';
						require("assets/php/friends/get_friend.php"); 
						if ($count == 0)
						{
							echo '<h1>You dont have any friends</h1>';
						}
						else 
						{
							echo '	<h1><center><table style="width:50%">';
							echo '	<tr>
									<th>User name</th>
									<th>PeerID</th>		
									</tr>';
							for ($i = 0; $i < $count; $i++) {
								echo '<tr>';
								echo '	<td>'; echo $friend_list[$i]['username']; echo '</td>
										<td>'; echo $friend_list[$i]['peerid']; echo '</td>';	
								echo '</tr>';
							}
							echo '</table></h1></center>';
						}
					}
					$_SESSION["message"] = '';
					?>
			</div>
		</section>
	</body>
</html>
